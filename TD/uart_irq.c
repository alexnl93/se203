#include "uart_irq.h"

#define UART0_C2    (*(volatile uint8_t *) 0x4006A003)
#define UART0_D     (*(volatile uint8_t *) 0x4006A007)
#define UART0_S1    (*(volatile uint8_t *) 0x4006A004)

extern rgb_color ima[8][8];
extern int led_number;
extern int led_color;
extern int row;
extern int line;
extern int estate;
extern int WE;

 /**
 * \file      uart_irq.c
 * \author    Alexnl022
 * \version   1.0
 * \date      3 février 2015 
 * \brief     Interruption recéption uart, ecrire dans une iamge
 */

/**
 * \brief activer les interruptions UART
 */

void uart_interupt_init()
{
  // Enable RX and TX and activate interrupt
  UART0_C2 = 0x2c;
  irq_enable(12);
  enable_irq();
}

/**
 * \brief incrementer une variable pour compter de min à max en boucle
 * \param  *num variable à incrément
 * \param  min valeur à partir de laquelle on compte
 * \param  max valeur jusqu'a laquelle on compte
 */

static void increment__number(int *num,int min, int max)
{
  if (*num < max)
    (*num)++;
  else
    (*num) = min;
}

/**
 * \brief choisir ce que l'on a a faire pendant l'interruption
 */

void UART0_IRQHandler()
{
  led_r_on();
  char c = UART0_D;
  
  if ((UART0_S1 & 0x2) || (UART0_S1 & 0x8))
    {
      WE = 0;
      //on nettoye les flags
      UART0_S1 |=(0x1<<1);
      UART0_S1 |=(0x1<<3);
    }

  if(c == 0xff)
    {
      estate=0;
      led_number = 0;
      WE=1;
    }
  
  switch (estate)
    {
    case 0:
      led_number = 0;
      led_color = 1;
      estate = 1;
      row = 0;
      line = 0;
      break;
    case 1:
      if (WE)
	{
	  switch(led_color)
	    {
	    case 1:
	      ima[line][row].r = c;
	      break;
	    case 2:
	      ima[line][row].g = c;
	      break;
	    case 3:
	      ima[line][row].b = c;
	      increment__number(&row,0,7);
	      break;
	    }
      
	  increment__number(&led_number,0,191);
	  increment__number(&led_color,1,3);
	  //changer de ligne à la fin de la ligne 
	  if ((row == 0)&&(led_color == 1))
	    increment__number(&line,0,7);
	  break;
	}
    }   
  led_r_off();
}


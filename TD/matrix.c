#include "matrix.h"

#define GPIOA_PCOR   (*(volatile uint32_t *) 0x400FF008)
#define GPIOA_PDDR   (*(volatile uint32_t *) 0x400FF014)
#define GPIOA_PSOR   (*(volatile uint32_t *) 0x400FF004)
#define GPIOB_PCOR   (*(volatile uint32_t *) 0x400FF048)
#define GPIOB_PDDR   (*(volatile uint32_t *) 0x400FF054)
#define GPIOB_PSOR   (*(volatile uint32_t *) 0x400FF044)
#define GPIOC_PCOR   (*(volatile uint32_t *) 0x400FF088)
#define GPIOC_PDDR   (*(volatile uint32_t *) 0x400FF094)
#define GPIOC_PSOR   (*(volatile uint32_t *) 0x400FF084)
#define GPIOD_PCOR   (*(volatile uint32_t *) 0x400FF0C8)
#define GPIOD_PDDR   (*(volatile uint32_t *) 0x400FF0D4)
#define GPIOD_PSOR   (*(volatile uint32_t *) 0x400FF0C4)
#define PORTA_PCR4   (*(volatile uint32_t *) 0x40049010)
#define PORTA_PCR12  (*(volatile uint32_t *) 0x40049030)
#define PORTA_PCR13  (*(volatile uint32_t *) 0x40049034)
#define PORTB_PCR0   (*(volatile uint32_t *) 0x4004A000)
#define PORTB_PCR1   (*(volatile uint32_t *) 0x4004A004)
#define PORTB_PCR2   (*(volatile uint32_t *) 0x4004A008)
#define PORTC_PCR8   (*(volatile uint32_t *) 0x4004B020)
#define PORTC_PCR9   (*(volatile uint32_t *) 0x4004B024)
#define PORTD_PCR2   (*(volatile uint32_t *) 0x4004C008)
#define PORTD_PCR4   (*(volatile uint32_t *) 0x4004C010)
#define PORTD_PCR5   (*(volatile uint32_t *) 0x4004C014)
#define PORTD_PCR6   (*(volatile uint32_t *) 0x4004C018)
#define PORTD_PCR7   (*(volatile uint32_t *) 0x4004C01C)
#define SIM_SCGC5    (*(volatile uint32_t *) 0x40048038)

extern char _binary_image_raw_start;

/**
 * \file      matrix.c
 * \author    Alexnl022
 * \version   1.0
 * \date      18 janvier 2015 
 * \brief     Gerer la matrice de LED
 */

/**
 * \brief Attendre durant un temps t
 * \param time Permet de rallonger la période d'attente, 1 cycle vaut 25ns si clock est à 40 MHz
 */

static inline void wait_matrix(int time)
{
  int i = 0; 
  for (i = 0; i < time  ; i++)
    asm volatile("nop");
}


/**
 * \brief Initialisation du tableau de led
 */

void matrix_init(void)
{
  //mise en marche des horloges
  SIM_SCGC5 |= (0x1<<9);
  SIM_SCGC5 |= (0x1<<10);
  SIM_SCGC5 |= (0x1<<11);
  SIM_SCGC5 |= (0x1<<12);

  //broches en mode GPIO
  PORTA_PCR4 = 0x00000103;
  PORTA_PCR12 = 0x00000103;  
  PORTA_PCR13 = 0x00000103;
  
  PORTB_PCR0 = 0x00000103;
  PORTB_PCR1 = 0x00000103;
  PORTB_PCR2 = 0x00000103;

  PORTC_PCR8 = 0x00000103;
  PORTC_PCR9 = 0x00000103;
  
  PORTD_PCR2 = 0x00000103;
  PORTD_PCR4 = 0x00000103;   
  PORTD_PCR5 = 0x00000103;
  PORTD_PCR6 = 0x00000103;
  PORTD_PCR7 = 0x00000103;

  // choix des broches en mode sortie
  GPIOA_PDDR = 0x00003010;
  GPIOB_PDDR = 0x00000007;
  GPIOC_PDDR = 0x00000300;
  GPIOD_PDDR = 0x000000F4;

  //initialisation des valeurs transmises
  GPIOA_PCOR = 0x00003010;
  GPIOB_PCOR = 0x00000004;
  GPIOC_PCOR = 0x00000300;
  GPIOD_PCOR = 0x000000F4;

  GPIOB_PSOR = 0x00000003;

  //attendre 0,2 secondes
  wait_matrix(8000000);

  //passer RST à l'etat haut
  GPIOB_PSOR = 0x00000004;

  init_bank0();
}

/**
 * \brief SET RST à 0 ou 1 (reset tout le dm163)
 * \param x qui vaut 0 ou 1
 */

void RST(int x)
{
  switch (x)
    {
    case 0:
      GPIOB_PCOR = (0x1<<2);
      break;
    case 1:
      GPIOB_PSOR = (0x1<<2);
      break;
    default:
      break;
    }
}

/**
 * \brief SET SB à 0 ou 1
 * \param x qui vaut 0 ou 1
 */

static void SB(int x)
{
  switch (x)
    {
    case 0:
      GPIOB_PCOR = (0x1<<0);
      break;
    case 1:
      GPIOB_PSOR = (0x1<<0);
      break;
    default:
      break;
    }
}

/**
 * \brief SET LAT à 0 ou 1
 * \param x qui vaut 0 ou 1
 */

static void LAT(int x)
{
  switch (x)
    {
    case 0:
      GPIOB_PCOR = (0x1<<1);
      break;
    case 1:
      GPIOB_PSOR = (0x1<<1);
      break;
    default:
      break;
    }
}

/**
 * \brief SET SCK à 0 ou 1
 * \param x qui vaut 0 ou 1
 */

static inline void SCK(int x)
{
  switch (x)
    {
    case 0:
      GPIOC_PCOR = (0x1<<8);
      break;
    case 1:
      GPIOC_PSOR = (0x1<<8);
      break;
    default:
      break;
    }
}

/**
 * \brief SET SDA à 0 ou 1
 * \param x qui vaut 0 ou 1
 */

static inline void SDA(int x)
{
  switch (x)
    {
    case 0:
      GPIOC_PCOR = (0x1<<9);
      break;
    case 1:
      GPIOC_PSOR = (0x1<<9);
      break;
    default:
      break;
    }
}

/**
 * \brief SET RWO à 0 ou 1
 * \param x qui vaut 0 ou 1
 */

static inline void ROW0(int x)
{
  switch (x)
    {
    case 0:
      GPIOA_PCOR = (0x1<<13);
      break;
    case 1:
      GPIOA_PSOR = (0x1<<13);
      break;
    default:
      break;
    }
}

/**
 * \brief SET RW1 à 0 ou 1
 * \param x qui vaut 0 ou 1
 */

static inline void ROW1(int x)
{
  switch (x)
    {
    case 0:
      GPIOD_PCOR = (0x1<<2);
      break;
    case 1:
      GPIOD_PSOR = (0x1<<2);
      break;
    default:
      break;
    }
}

/**
 * \brief SET RW2 à 0 ou 1
 * \param x qui vaut 0 ou 1
 */

static inline void ROW2(int x)
{
  switch (x)
    {
    case 0:
      GPIOD_PCOR = (0x1<<4);
      break;
    case 1:
      GPIOD_PSOR = (0x1<<4);
      break;
    default:
      break;
    }
}

/**
 * \brief SET RW2 à 0 ou 1
 * \param x qui vaut 0 ou 1
 */

static inline void ROW3(int x)
{
  switch (x)
    {
    case 0:
      GPIOD_PCOR = (0x1<<6);
      break;
    case 1:
      GPIOD_PSOR = (0x1<<6);
      break;
    default:
      break;
    }
}

/**
 * \brief SET RW4 à 0 ou 1
 * \param x qui vaut 0 ou 1
 */

static inline void ROW4(int x)
{
  switch (x)
    {
    case 0:
      GPIOD_PCOR = (0x1<<7);
      break;
    case 1:
      GPIOD_PSOR = (0x1<<7);
      break;
    default:
      break;
    }
}

/**
 * \brief SET RW5 à 0 ou 1
 * \param x qui vaut 0 ou 1
 */

static inline void ROW5(int x)
{
  switch (x)
    {
    case 0:
      GPIOD_PCOR = (0x1<<5);
      break;
    case 1:
      GPIOD_PSOR = (0x1<<5);
      break;
    default:
      break;
    }
}

/**
 * \brief SET RW6 à 0 ou 1
 * \param x qui vaut 0 ou 1
 */

static inline void ROW6(int x)
{
  switch (x)
    {
    case 0:
      GPIOA_PCOR = (0x1<<12);
      break;
    case 1:
      GPIOA_PSOR = (0x1<<12);
      break;
    default:
      break;
    }
}

/**
 * \brief SET RW7 à 0 ou 1
 * \param x qui vaut 0 ou 1
 */

static inline void ROW7(int x)
{
  switch (x)
    {
    case 0:
      GPIOA_PCOR = (0x1<<4);
      break;
    case 1:
      GPIOA_PSOR = (0x1<<4);
      break;
    default:
      break;
    }
}

/**
 * \brief Faire 1 pulse negatif
 */

static inline void pulse_LAT(void)
{
  LAT(1);
  wait_matrix(2);
  LAT(0);
  wait_matrix(1);
  LAT(1);
  wait_matrix(2);
}

/**
 * \brief Eteindre toute les lignes
 */

void desactivate_rows(void)
{
  ROW0(0);
  ROW1(0);
  ROW2(0);
  ROW3(0);
  ROW4(0);
  ROW5(0);
  ROW6(0);
  ROW7(0);
}

/**
 * \brief active la ligne dont le numéro est passé en argument
 * \param row numero de la ligne à activer
 */

static inline void activate_row(int row)
{
  switch (row)
    {
    case 0:
      ROW0(1);
      break;
    case 1:
      ROW1(1);
      break;
    case 2:
      ROW2(1);
      break;
    case 3:
      ROW3(1);
      break;
    case 4:
      ROW4(1);
      break;
    case 5:
      ROW5(1);
      break;
    case 6:
      ROW6(1);
      break;
    case 7:
      ROW7(1);
      break;
    default:
      break;
    }
}

/**
 * \brief Valeur d'un bit sur un octet
 * \param val octet ou l'on doit trouver la valeur du bit
 * \number numero du bit à trouver
 */

static inline int  bit_value(uint8_t val, int number)
{
  int i;
  i = val & (0x1<<number);
  i = i && 1;
  return i;
}

/**
 * \brief Envoyer un byte d'information (=une couleur de led)
 * \param val octet à envoyer
 * \param bank  quelle est la bank concerné: bank0 ou bank1
 */

static inline void send_byte(uint8_t val, int bank)
{
  int m,i;
  
  if (bank == 0)
    {
      m = 6;
      SB(0);
    }
  else
    {
      m = 8;
      SB(1);
    }

  for (i = m-1; i >= 0; i--)
    {
      SDA(bit_value(val,i));
      wait_matrix(2);
      SCK(1);
      wait_matrix(2);
      SCK(0);	   
    }
}

/**
 * \brief Envoyer un bit d'information
 * \param *val tableau de couleur des 8 pixels
 * \param row rangée à allumer
 */

static inline void mat_set_row(int row, const rgb_color *val)
{
  int i;
  for(i=7;i>=0;i--)
    {
      rgb_color buf = val[i];
      send_byte(buf.b,1);
      send_byte(buf.g,1);
      send_byte(buf.r,1);
      
    }
  pulse_LAT();
  activate_row(row);
}

/**
 * \brief Mettre tout les bites de bank0 à 1
 */

void init_bank0(void)
{
  uint8_t buf = 0xff;
  int i;
  for(i=0;i<24;i++)
    {
      send_byte(buf,0);
    }
   pulse_LAT();
}

/**
 * \brief Afficher l'image à partir de la sturcutre image
 * \param **ima : le pointer de structure
  */

void display_image(rgb_color ima[8][8])
{
  int i;
  int j;

  for(j = 0; j < 3; j++)
    {
      //8 car 8 colonne
      for(i=0;i<8;i++)
	{
	  mat_set_row(i,ima[i]);
	  wait_matrix(500);
	  desactivate_rows();
	}
    }
}
/**
 * \brief convertir un tableau de char en une strucutre image 
 * \param *tab : le pointer de tableau
 * \param ima[8][8] : la strucutre de recpopi 
  */

void convert_image(char *tab, rgb_color ima[8][8])
{
  int i;
  int j;

  for(i=0;i<8;i++)
    {
      for(j=0;j<8;j++)
	{
	  ima[i][j].r = tab[3*j+24*i];
	  ima[i][j].g = tab[3*j+24*i+1];
	  ima[i][j].b = tab[3*j+24*i+2];
	}
    }
  
}

/**
 * \brief Afficher l'image à partir de la sturcutre de donnée brut (led0: R0,G0,B0,... led[191] R...)
 * \param *data_struct : le pointer de structure
  */ 
void display_image_from_data_struct(char data_struct[192])
{
  rgb_color ima[8][8];
  convert_image(data_struct,ima);
  display_image(ima);
}

/**
 * \brief Tester le driver du colorshield
 */

void test_pixels(void)
{
  rgb_color val[8];

  val[0].r=0;
  val[0].g=0;
  val[0].b=0;

  val[1].r=0;
  val[1].g=0;
  val[1].b=36;
  
  val[2].r=0;
  val[2].g=0;
  val[2].b=72;
  
  val[3].r=0;
  val[3].g=0;
  val[3].b=108;

  val[4].r=0;
  val[4].g=0;
  val[4].b=144;

  val[5].r=0;
  val[5].g=0;
  val[5].b=180;

  val[6].r=0;
  val[6].g=0;
  val[6].b=216;
  
  val[7].r=0;
  val[7].g=0;
  val[7].b=252;

  int i;
  for(i=0;i<8;i++)
    {
      mat_set_row(i,val);
      wait_matrix(10000000);
      desactivate_rows();
    }

  val[0].r=0;
  val[0].b=0;
  val[0].g=0;

  val[1].r=0;
  val[1].b=0;
  val[1].g=36;
  
  val[2].r=0;
  val[2].b=0;
  val[2].g=72;
  
  val[3].r=0;
  val[3].b=0;
  val[3].g=108;

  val[4].r=0;
  val[4].b=0;
  val[4].g=144;

  val[5].r=0;
  val[5].b=0;
  val[5].g=180;

  val[6].r=0;
  val[6].b=0;
  val[6].g=216;
  
  val[7].r=0;
  val[7].b=0;
  val[7].g=252;

  for(i=0;i<8;i++)
    {
      mat_set_row(i,val);
      wait_matrix(10000000);
      desactivate_rows();
    }
    
  val[0].g=0;
  val[0].b=0;
  val[0].r=0;

  val[1].g=0;
  val[1].b=0;
  val[1].r=36;
  
  val[2].g=0;
  val[2].b=0;
  val[2].r=72;
  
  val[3].g=0;
  val[3].b=0;
  val[3].r=108;

  val[4].g=0;
  val[4].b=0;
  val[4].r=144;

  val[5].g=0;
  val[5].b=0;
  val[5].r=180;

  val[6].g=0;
  val[6].b=0;
  val[6].r=216;
  
  val[7].g=0;
  val[7].b=0;
  val[7].r=252;

  for(i=0;i<8;i++)
    {
      mat_set_row(i,val);
      wait_matrix(10000000);
      desactivate_rows();
    }
}

/**
 * \brief Tester une led en particulier
 * \param c la couleur: 'r' : rouge, 'b':bleu, 'g':green
 * \param number : numéro de la rangé concernée
 */

void test_couleur(char c, int number)
{
  int k;
  rgb_color val[8];
  
  for(k=0;k<8;k++)
    {
      val[k].r=0;
      val[k].g=0;
      val[k].b=0;
    }
  
  switch(c)
    {
    case 'r':
      val[number].r=255;
      break;
    case 'g':
      val[number].g=255;
      break;
    case 'b':
      val[number].b=255;
      break;
    default:
      break;
    }

  for(k=0;k<8;k++)
    {
      mat_set_row(k,val);
    }
}

/**
 * \brief Afficher une image à partir d'un fichier binaire dont on a le le nom du pointeur dans .data
 */

void test_image(void)
{
  display_image_from_data_struct(&_binary_image_raw_start);
}


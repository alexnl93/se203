#include "led.h"

#define SIM_SCGC5   (*(volatile uint32_t *) 0x40048038)
#define PORTD_PCR5  (*(volatile uint32_t *) 0x4004C014)
#define PORTE_PCR29 (*(volatile uint32_t *) 0x4004D074)
#define GPIOD_PDDR  (*(volatile uint32_t *) 0x400FF0D4)
#define GPIOE_PDDR  (*(volatile uint32_t *) 0x400FF114)
#define GPIOD_PCOR  (*(volatile uint32_t *) 0x400FF0C8)
#define GPIOE_PCOR  (*(volatile uint32_t *) 0x400FF108)
#define GPIOD_PSOR  (*(volatile uint32_t *) 0x400FF0C4)
#define GPIOE_PSOR  (*(volatile uint32_t *) 0x400FF104)
#define GPIOD_PTOR  (*(volatile uint32_t *) 0x400FF0CC)
#define GPIOE_PTOR  (*(volatile uint32_t *) 0x400FF10C)

 /**
 * \file      led.c
 * \author    Alexnl022
 * \version   1.0
 * \date      6 janvier 2015 
 * \brief     Périphérique led
 */

/**
 * \brief initialisation des deux leds
 * \brief rouge E et vert D
 * \brief allumage des diode à la fin de l'initialisation
 */

void led_init(void)
{
  SIM_SCGC5 = SIM_SCGC5 | 0x3000;

  PORTD_PCR5 = PORTD_PCR5 & 0xfffff8ff;
  PORTD_PCR5 = PORTD_PCR5 | 0x100;

  PORTE_PCR29 = PORTE_PCR29 & 0xfffff8ff;
  PORTE_PCR29 = PORTE_PCR29 | 0x100;

  GPIOD_PDDR = GPIOD_PDDR | (0x1<<5);
  GPIOE_PDDR = GPIOE_PDDR | (0x1<<29);  

  GPIOD_PCOR = (0x1<<5);
  GPIOE_PCOR = (0x1<<29);
}

/**
 * \brief initialisation la ld rouge
 */

void led_init_r(void)
{
  SIM_SCGC5 = SIM_SCGC5 | 0x3000;

  PORTE_PCR29 = PORTE_PCR29 & 0xfffff8ff;
  PORTE_PCR29 = PORTE_PCR29 | 0x100;

  GPIOE_PDDR = GPIOE_PDDR | (0x1<<29);  

  GPIOE_PCOR = (0x1<<29);
}

/**
 * \brief Allumer les deux leds
 */

void led_on(void)
{
  GPIOD_PCOR = (0x1<<5);
  GPIOE_PCOR = (0x1<<29); 

}

/**
 * \brief Eteindre les deux leds
 */

void led_off(void)
{
  GPIOD_PSOR = (0x1<<5);
  GPIOE_PSOR = (0x1<<29); 
}

/**
 * \brief Changer d'etats les deux leds
 * \brief elles s'allument si elles étaient eteinte ou vice versa
 */

void led_toggle(void)
{
  GPIOD_PTOR = (0x1<<5); 
  GPIOE_PTOR = (0x1<<29); 
}

/**
 * \brief allumer la led rouge
 */

void led_r_on(void)
{
  GPIOE_PCOR = (0x1<<29); 
}

/**
 * \brief Allumer la led verte
 */

void led_g_on(void)
{
  GPIOD_PCOR = (0x1<<5); 
}

/**
 * \brief Eteindre la led rouge
 */


void led_r_off(void)
{
  GPIOE_PSOR = (0x1<<29); 
}

/**
 * \brief Eteindre la led verte
 */

void led_g_off(void)
{
  GPIOD_PSOR = (0x1<<5);
}

/**
 * \brief changer d'etats la led rouge
 */

void led_r_toggle(void)
{
  GPIOE_PTOR = (0x1<<29); 
}


/**
 * \brief changer d'etats la led rouge
 */


void led_g_toggle(void)
{
  GPIOD_PTOR = (0x1<<5);
}

/**
 * \brief Attendre durant un temps t
 * \param time Permet de rallonger la période d'attente, 1millions envirion 2 sevondes
 */

void wait(int time)
{
  int i = 0; 
  for (i = 0; i < time  ; i++)
    asm volatile("nop");
}

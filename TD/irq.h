#ifndef IRQ_H
#define IRQ_H

#define enable_irq()  do {asm volatile ("cpsie i");} while (0)
#define disable_irq() do {asm volatile ("cpsid i");} while (0)

#include <stdint.h>

/**
 * \brief le nom doit être de la forme  XXX_IRQHandler
 */ 

void irq_init(void);
void irq_enable(int irq_number);
void irq_disable(int irq_number);

#endif

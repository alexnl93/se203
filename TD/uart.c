#include "uart.h"

#define PORTA_PCR1  (*(volatile uint32_t *) 0x40049004)
#define PORTA_PCR2  (*(volatile uint32_t *) 0x40049008)
#define SIM_SOPT2   (*(volatile uint32_t *) 0x40048004)
#define SIM_SCGC4   (*(volatile uint32_t *) 0x40048034)
#define SIM_SCGC5   (*(volatile uint32_t *) 0x40048038)
#define UART0_BDH   (*(volatile uint8_t *) 0x4006A000)
#define UART0_BDL   (*(volatile uint8_t *) 0x4006A001)
#define UART0_C1    (*(volatile uint8_t *) 0x4006A002)
#define UART0_C2    (*(volatile uint8_t *) 0x4006A003)
#define UART0_C3    (*(volatile uint8_t *) 0x4006A006)
#define UART0_C4    (*(volatile uint8_t *) 0x4006A00A)
#define UART0_D     (*(volatile uint8_t *) 0x4006A007)
#define UART0_MA1   (*(volatile uint8_t *) 0x4006A008)
#define UART0_MA2   (*(volatile uint8_t *) 0x4006A009)
#define UART0_S1    (*(volatile uint8_t *) 0x4006A004)
#define UART0_S2    (*(volatile uint8_t *) 0x4006A005)

#define UART0_S1_TDRE_MASK 0x80
#define UART0_S1_TC_MASK   0x40
#define UART0_S1_RDRF_MASK 0x20

/**
 * \file      uart.c
 * \author    Alexnl022
 * \version   1.0
 * \date      9 janvier 2015
 * \brief     Envoyer des caracteres sur le port série uart
 */

/**
 * \brief Initialisation du port serie UART0
 * \brief cf excel pour les valeurs choisies
 */

void uart_init(void)
{
  SIM_SOPT2 &= 0xf3ffffff;
  SIM_SOPT2 |= (0x1<<26);
  SIM_SOPT2 |= (0x1<<16);

  SIM_SCGC4 |= (0x1<<10);

  //nettoyage des flgs conseiller par le quickstart
  UART0_C1 = 0x00; //correspond bien à ce qu'il faut

  UART0_C3 = 0x00;
  UART0_MA1 = 0x00;
  UART0_MA2 = 0x00;
  UART0_S1 |=  0x1F;
  UART0_S2 |=  0xC0;

  UART0_C4 = 24;  // OSR a 25, soit uart0_C4 = 24

  // SBR a 7 sur 13 bit les 5 bits de poid fort = 5 bits de poids faible de BDH
  UART0_BDH = 0x00;

  UART0_BDL = 25; // 8 bits de poid faible de SBR

  SIM_SCGC5 |= (0x1<<9);

  PORTA_PCR1 = 0x00000203;
  PORTA_PCR2 = 0x00000203;

  // Clear error flags
  UART0_S1 = 0xFF;

  // Enable RX and TX
  UART0_C2 = 0x0c;

  // Empty RX buffer
  UART0_D;
}

/**
 * \brief  Attendre que l'UART soit prêt à transmettre, puis demande d'envoye
 * \param  c  Elément à transmettre
 */

void uart_putchar(char c)
{
  while(!(UART0_S1 & UART0_S1_TDRE_MASK));
  UART0_D = c;
}

/**
 * \brief Attendre que l'UART ait reçu un caractère puis le retourne
 */

unsigned char uart_getchar(void)
{
  // Test if OR
  if (UART0_S1 & 0x8)
      while (1)
          asm volatile ("nop");

  while(!(UART0_S1 & UART0_S1_RDRF_MASK));

  // Test if FE
  if (UART0_S1 & 0x2)
      while (1)
          asm volatile ("nop");

  return UART0_D;
}

/**
 * \brief Meme signification que puts sous linux
 * \param *s pointeur de tableau
 */

void uart_puts(const char *s)
{
  while(*s != 0)
      uart_putchar(*s++);
  uart_putchar('\n');
}

/**
 * \brief Meme signification que gets sous linux
 * \param *s pointeur de tableau
 * \param size nombre de lettre du mot
 */

void uart_gets(char *s, int size)
{
  int i = 0;
  for(i=0;i<size-1;i++)
    {
      s[i] = uart_getchar();
    }
  s[i+1]='\0';
}

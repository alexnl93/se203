#include <stdint.h>
#include <stdio.h>

int32_t x = 34;
int8_t y;
const char  w = 3;
void fonction();

int main()
{
static uint8_t z;
printf("Stack1: z = %p\n", (void*) &z);
printf(".bss: y  = %p\n",(void*) &y);
printf(".data: x = %p\n",(void*) &x);
 
printf(".text: fonction = %p\n", (void*) &fonction);
printf(".rodata: w = %p\n",(void*) &w);
static uint8_t u;
printf("Stack2: u = %p\n", (void*) &u);
 
return 0;
}


void fonction(void){}

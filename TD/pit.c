#include "pit.h"

#define PIT_LDVAL0  (*(volatile uint32_t *) 0x40037100) 
#define PIT_MCR     (*(volatile uint32_t *) 0x40037000)
#define PIT_TCTRL0  (*(volatile uint32_t *) 0x40037108)
#define PIT_TFLG0   (*(volatile uint32_t *) 0x4003710C)
#define SIM_SCGC6   (*(volatile uint32_t *) 0x4004803C)

/**
 * \file      uart.c
 * \author    Alexnl022
 * \version   1.0
 * \date      3 février 2015
 * \brief     Module pour gérer les interruptions répété à intervalle de temps constant
 */

/**
 * \brief initialisation des interruptions pédirodiques 
 */

void pit_init(void)
{
  led_init_r();
  
  //activation des horloges
  SIM_SCGC6 |= (0x1<<23);
  PIT_MCR = 0;
  
  //periode 50 ns
  //1/70 seconde -> valeur < 2*10⁷/70
  //prendre 200000
  PIT_LDVAL0 = 100000;
  
  //activer le timer
  PIT_TCTRL0 = 0x00000003;
  
  //nettoyer le flasg d'interruption
  PIT_TFLG0 = 1;
  
  irq_enable(22);
  enable_irq();
}
/**
 * \brief Handler à executer 
 */

void PIT_IRQHandler(void)
{
  extern rgb_color ima[8][8];
  //nettoyer le flasg d'interruption
  PIT_TFLG0 = 1;
  display_image(ima);
}


#include "buttons.h"
#include "irq.h"
#include "led.h"
#include "matrix.h"
#include "uart.h"
#include "uart_irq.h"

extern void clocks_init(void);

rgb_color ima[8][8];
volatile int led_number = 0;
volatile int led_color = 1;
volatile int row = 0;
volatile int line = 0;
volatile int estate = 0;

//write enable
volatile int WE = 1;

int main()
{
  clocks_init(); 
  
  /*
  disable_irq();
  irq_init();
  led_init_r();
  led_r_on();
  button_init();
  matrix_init();
  uart_init();
  uart_interupt_init();
  */
  
  int i = 0;

  led_init();

  for (i = 0; i < 10  ; i++)
  {
    wait(6000000);
    led_r_toggle();
    led_g_toggle();
  }
  
  //  while(1)
  //  display_image(ima);
  return 0;
}




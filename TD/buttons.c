#include "buttons.h"
#include "led.h"

#define SIM_SCGC5    (*(volatile uint32_t *) 0x40048038)
#define PORTC_ISFR   (*(volatile uint32_t *) 0x4004B0A0)
#define PORTC_PCR3   (*(volatile uint32_t *) 0x4004B00C)
#define GPIOC_PDDR   (*(volatile uint32_t *) 0x400FF094)

 /**
 * \file      led.c
 * \author    Alexnl022
 * \version   1.0
 * \date      27 janvier 2015 
 * \brief     Interruption boutons pousssoir, marche LED
 */

/**
 * \brief acitvation interruption
 */

void button_init()
{   
  SIM_SCGC5 |= (0x1<<11);
  
  //flag isf que je met à 0 (seulement pour un clear) p199
  //interrupt on a rising hedge
  //le trois à la fin correspond au mode pull-up
  //bouton non appuyé sur 1
  PORTC_PCR3 = 0x000a0103;

  //configuration entré/sortie du port c
  GPIOC_PDDR = 0x00000000;
 
  irq_enable(31);
  enable_irq();
}

void Pin_C_and_D_IRQHandler()
{ 
  //arreter l'interuption
  PORTC_ISFR = (0x1<<3);
  led_r_toggle();
}

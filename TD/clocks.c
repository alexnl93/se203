#include "clocks.h"

#define MCG_C1       (*(volatile uint8_t *)  0x40064000)
#define MCG_C2       (*(volatile uint8_t *)  0x40064001)
#define MCG_C5       (*(volatile uint8_t *)  0x40064004)
#define MCG_C6       (*(volatile uint8_t *)  0x40064005)
#define MCG_S        (*(volatile uint8_t *)  0x40064006)
#define SIM_CLKDIV1  (*(volatile uint32_t *) 0x40048044)
#define SIM_COPC     (*(volatile uint32_t *) 0x40048100)

 /**
 * \file      clocks.c
 * \author    Alexnl022
 * \version   1.0
 * \date      9 février 2015 
 * \brief     Interruptions
 */

/**
 * \brief Etre dans le bon mode d'horloge pour avoir la bonne vitesse de MGCOUTCLK
 */

void clocks_init()
{  
  int i = 0;
  
  //Ne pas avoir de Watch-dogs
  //initalisé à 12 normalement
  //plus d'erreur dans les for
  SIM_COPC = 0;

  MCG_C2=0x2C;
  
  //c1[FRDIV] sur 011 pour diviser par 256 et non 128
  // pour compenser le quartz à 8 MhZ
  MCG_C1=0x98;

  for (i = 0 ; i < 20000 ; i++)
    {
      if (MCG_S & 0x02) break; 
    }

  for (i = 0 ; i < 2000 ; i++)
    {
      if (!(MCG_S & 0x10)) break; 
    }

  for (i = 0 ; i < 2000 ; i++)
    {
      if (((MCG_S & 0x0c) >> 2) == 0x2) break; 
    }
  
  //ETAT FBE

  //on choisit 4 au lieu de 2 car 8MHz
  //on obtient une PLL à 2 MHz
  MCG_C5=0x03;

  //CME0 est mis à 0,il ne permet plus de déclencher des interruptions 
  MCG_C6=0x40;

  for (i = 0 ; i < 2000 ; i++)
    {
      if (MCG_S & 0x20) break; 
    }
  
  for (i = 0 ; i < 4000 ; i++)
    {
      if (MCG_S & 0x40) break; 
    }

  //ETAT PBE

  //on change le clock source select à verifier
  MCG_C1=0x18;

  for (i = 0 ; i < 2000 ; i++)
    {
      if (((MCG_S & 0x0c) >> 2) == 0x3) break; 
    }

  //ETAT PEE

   //division par 1 (core clock) et 2 (flash clock)
  //retour au niveau d'energie bas
  SIM_CLKDIV1 = 0x00010000;
}

#ifndef BUTTONS_H
#define BUTTONS_H

#include <stdint.h>
#include "irq.h"
#include "led.h"

void button_init(void);
void Pin_C_and_D_IRQHandler(void);

#endif

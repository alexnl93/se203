#ifndef UART_IRQ__H
#define UART_IRQ__H

#include <stdint.h>
#include "irq.h"
#include "uart.h"
#include "matrix.h"
#include "led.h"

void uart_interupt_init(void);
void UART0_IRQHandler(void);

#endif

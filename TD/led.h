#ifndef LED_H
#define LED_H

#include <stdint.h>

void led_init(void);
void led_init_r(void);
void led_on(void);
void led_off(void);
void led_toggle(void);
void led_r_on(void);
void led_g_on(void);
void led_r_off(void);
void led_g_off(void);
void led_r_toggle(void);
void led_g_toggle(void);
void wait(int time);

#endif

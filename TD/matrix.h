#ifndef MATRIX_H
#define MATRIX_H

#include <stdint.h>

typedef struct {
  unsigned char r;
  unsigned char g;
  unsigned char b;
} rgb_color;

//les signaux
void matrix_init(void);
void RST(int x);

//controle bas niveau
void desactivate_rows(void);
void init_bank0(void);

//controle affichage
void display_image(rgb_color ima[8][8]);
void convert_image(char *tab, rgb_color ima[8][8]);
void display_image_from_data_struct(char data[192]);

//test
void test_pixels(void);
void test_couleur(char c, int number);
void test_image(void);
  
#endif

extern char _etext, _data, _edata,_bstart, _bend;

void init_bss()
{
  char *src = &_etext;

  char *dst ;

  //copie des .data en RAM
  for (dst = &_data; dst < &_edata; dst++)
    *dst = *src++; 

  //on initialise le bss à 0
  for (dst = &_bstart; dst < &_bend; dst++)
    *dst = 0;
 
}
